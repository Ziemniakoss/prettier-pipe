# Bitbucket Pipelines Pipe: Prettier validation report

Pipe for prettier validation. Creates report.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yml
- pipe: docker://ziemniakoss/prettier-pipe:1.10
  variables:
    PRETTY_FILES: "<string>"
    PRETTY_REPORT_TYPE: "<string>"
    PRETTY_REPORT_ID: "<string>"
    PRETTY_REPORT_TITLE: "<string>"
    PRETTY_REPORT_DETAILS_SUCCESS: "<string>"
    PRETTY_REPORT_DETAILS_FAILURE: "<string>"
    PRETTY_ANNOTATION_TYPE: "<string>"
    PRETTY_ANNOTATION_SEVERITY: "<string>"
    PRETTY_ANNOTATION_SUMMARY: "<string>"
```

## Variables

| Variable                      | Default value                        | Usage                                                                                                                                 |
| ----------------------------- | ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------- |
| PRETTY_FILES                  | .                                    | Files to scan. By default it will try to scan all files in repo (this will call prettier --list-different .)                          |
| PRETTY_REPORT_TYPE            | BUG                                  | Type of created report. Possible values: <ul><li>SECURITY</li><li>COVERAGE</li><li>TEST</li><li>BUG</li></ul>                         |
| PRETTY_REPORT_ID              | pretty                               | Unique id of report. Every commit can have only one report with same id                                                               |
| PRETTY_REPORT_TITLE           | Prettier report                      | Title of report. This text will be displayed in code insights                                                                         |
| PRETTY_REPORT_DETAILS_SUCCESS | All files are properly formatted     | This text will be displayed in report preview if no formatting violations will be found                                               |
| PRETTY_REPORT_DETAILS_FAILURE | {count} files were not formatted     | This text will be displayed in report preview if some violations were found. {count} will be replaced with total number of violations |
| PRETTY_ANNOTATION_TYPE        | CODE_SMELL                           | Annotation type. Possible values <ul><li>VULNERABILITY</li><li>CODE_SMELL</li><li>BUG</li>                                            |
| PRETTY_ANNOTATION_SEVERITY    | LOW                                  | Severity of annotations. Possible values: <ul><li>HIGH</li><li>MEDIUM</li><li>LOW</li><li>CRITICAL</li></ul>                          |
| PRETTY_ANNOTATION_SUMMARY     | This file is not formatted correctly | Text displayed in as title of annotation                                                                                              |

## Details

This pipe will scan files in repository for formatting errors with prettier. After scanning, it will submit report with
annotations.

This pipe should work out of the box. By default, it will scan all files supported by prettier. If you want to use
plugins, make sure you have package.json file in root folder of repository.

## Prerequisites

None

## Examples

Add the following snippet to the script section of your bitbucket-pipelines.yml file.

```yml
- step:
    script:
      - pipe: docker://ziemniakoss/prettier-pipe:1.10
```

## Support

Its university project so I am not planning to support it.
