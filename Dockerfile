FROM python:3.7
RUN apt update && apt install nodejs npm -y
run npm i -g prettier
COPY requirements.txt /
RUN pip install -r requirements.txt
COPY pipe.py /
ENTRYPOINT ["python3", "/pipe.py"]