import datetime
import os
import subprocess
from typing import List
from uuid import uuid4

from bitbucket_pipes_toolkit import Pipe, CodeInsights

VARS = {
	'BITBUCKET_USERNAME': {
		'type': 'string',
		'required': False,
		'default': os.getenv('BITBUCKET_REPO_OWNER')
	},
	'BITBUCKET_REPOSITORY': {
		'type': 'string',
		'required': False,
		'default': os.getenv('BITBUCKET_REPO_SLUG')
	},
	"PRETTY_FILES": {
		'type': 'string',
		'required': False,
		'default': "."
	},
	"PRETTY_REPORT_TYPE": {
		'type': 'string',
		'required': False,
		'default': 'BUG'
	},
	"PRETTY_REPORT_ID": {
		'type': 'string',
		'required': False,
		'default': 'pretty'
	},
	"PRETTY_REPORT_TITLE": {
		'type': 'string',
		'required': False,
		'default': 'Prettier report'
	},
	"PRETTY_REPORT_DETAILS_SUCCESS": {
		'type': 'string',
		'required': False,
		'default': 'All files are properly formatted'
	},
	"PRETTY_REPORT_DETAILS_FAILURE": {
		'type': 'string',
		'required': False,
		'default': '{count} files were not formatted'
	},
	"PRETTY_ANNOTATION_TYPE": {
		"type": "string",
		"required": False,
		"default": "CODE_SMELL"
	},
	"PRETTY_ANNOTATION_SEVERITY": {
		"type": "string",
		"required": False,
		"default": "LOW"
	},
	"PRETTY_ANNOTATION_SUMMARY": {
		"type": "string",
		"required": False,
		"default": "This file is not formatted correctly"
	},
	"DEBUG": {
		"type": "boolean",
		"required": False,
		"default": False
	}

}
pipe = Pipe(schema=VARS)

REPORT_ID = pipe.get_variable("PRETTY_REPORT_ID")

COMMIT = os.getenv('BITBUCKET_COMMIT')


def get_insights():
	return CodeInsights(
		repo=pipe.get_variable('BITBUCKET_REPOSITORY'),
		username=pipe.get_variable('BITBUCKET_USERNAME')
	)


def upsert_report(failsCount, scanning_time_ms: int):
	insights = get_insights()

	details = pipe.get_variable("PRETTY_REPORT_DETAILS_SUCCESS")
	result = "PASSED"
	if failsCount > 0:
		details = pipe.get_variable("PRETTY_REPORT_DETAILS_FAILURE").replace("{count}", str(failsCount))
		result = "FAILED"
	data = [
		{
			"title": "Not formatted files",
			"type": "NUMBER",
			"value": failsCount
		}
	]
	report_data = {
		"type": "report",
		"report_type": pipe.get_variable("PRETTY_REPORT_TYPE"),
		"title": pipe.get_variable("PRETTY_REPORT_TITLE"),
		"details": details,
		"result": result,
		"external_id": REPORT_ID,
		"data": data
	}
	pipe.log_debug(f"SENDING report {report_data}")
	report = insights.create_report(COMMIT, report_data=report_data)
	pipe.log_debug("Report created")
	return report


def create_annotation_for(fileName: str):
	return {
		"annotation_type": pipe.get_variable("PRETTY_ANNOTATION_TYPE"),
		"external_id": str(uuid4()),
		"summary": "This file is not formatted correctly",
		"severity": pipe.get_variable("PRETTY_ANNOTATION_SEVERITY"),
		"result": "FAILED",
		"line": 0,
		"path": fileName
	}


def annotate_files(files):
	insights = get_insights()
	if len(files) == 0:
		pipe.log_info("No errors were found")
		return
	annotations = [create_annotation_for(file) for file in files]
	for annotation in annotations:
		pipe.log_debug(f"Creating annotation {annotation}")
		insights.create_annotation(COMMIT, REPORT_ID, annotation)
		pipe.log_debug("Annotation created")


def find_unformatted_files() -> List[str]:
	filesStr = pipe.get_variable("PRETTY_FILES")
	pipe.log_debug(f"Finding files matching {filesStr}")
	output  = subprocess.run(
		["prettier", "--no-error-on-unmatched-pattern", "--list-different", filesStr],
		stdout=subprocess.PIPE
	).stdout.decode().strip()
	if output == '':
		return []
	files = output.split("\n")
	pipe.log_debug(f"Found {files}")

	return files


def install_npm_packages():
	pipe.log_info("Installing npm dependencies")
	if os.path.exists("package.json"):
		output = subprocess.check_output(["npm", "i"])
		pipe.log_debug(output.decode())
	else:
		pipe.log_debug("package.json does not exist, skip npm packages installation")


def main():
	if pipe.get_variable("DEBUG"):
		pipe.enable_debug_log_level()

	install_npm_packages()
	pipe.log_info("Scanning for wrongly formatted files")
	filesWithIncorrectFormatting = []
	start = datetime.datetime.now()
	try:
		filesWithIncorrectFormatting = find_unformatted_files()
	except Exception as e:
		pipe.fail(f"Error occurred while scanning with prettier: {str(e)}")
	end = datetime.datetime.now()
	duration = end - start
	duration_in_ms = duration.microseconds // 1000
	pipe.log_info(f"Scanning complete in {duration_in_ms} ms, found {len(filesWithIncorrectFormatting)}")

	failsCount = len(filesWithIncorrectFormatting)
	pipe.log_info("Upserting report")
	try:
		upsert_report(failsCount, duration_in_ms)
	except Exception as e:
		pipe.log_error("Error occurred while upserting report")
		pipe.fail(str(e))

	if failsCount == 0:
		pipe.success("All files are properly formatted", True)

	pipe.log_info("Creating annotations")
	try:
		annotate_files(filesWithIncorrectFormatting)
	except Exception as e:
		pipe.log_error("Error occurred while creating annotations")
		pipe.fail(str(e))
	pipe.fail("Formatting errors found")


if __name__ == '__main__':
	main()
